> ### Joyjet  - Tech interview tests - Backend (Laravel codebase containing (API, CRUD, auth) .

----------

# Base Stack Versions

Devilbox v1.9.2
PHP 7.4.20
Nginx 1.18.0
Mariadb 10.5.8

# Getting started

## Clone Repository

git clone https://pedrosoja76@bitbucket.org/pedrosoja76/pedrosoja-tech-interview-tests.git
## Docker

To install with [Devilbox v1.9.2](https://devilbox.readthedocs.io/en/latest/examples/setup-laravel.html), run following commands:

Follow this documentation to instal the [Docker Desktop](https://docs.docker.com/desktop/)

sudo service docker start

Clone the repository Devilbox

```
git clone https://github.com/cytopia/devilbox

Switch to the repo folder

cd devilbox
Copy the example env file and make the required configuration changes in the .env file

cp env-example .env

Check the host machine user and group id
id -u
id -g

Edit the .env file wiht the parameters
vi .env (NEW_UID={user_id} // NEW_GID={group_id} // HOST_PATH_HTTPD_DATADIR={inform the directory Joyjet from Repository})

Start the docker
docker-compose up -d httpd php mysql

Access the Docker shell cli
./shell.sh

```

## Installation

[Official Documentation](https://laravel.com/docs/7.x)

```
Insider the Devilbox Docker, install the Laravel Project
laravel new joyjet

Change into the directory project
cd joyjet

Create a symbolic link for Devilbox virtualhost
ln -s public htdocs

Run the database migrations (**Set the database connection in .env before migrating**)
php artisan migrate
```

# Code overview


## Folders

- `app` - Contains all the Eloquent models
- `app/Http/Controllers/Api` - Contains all the api controllers
- `app/Http/Requests/ArticleStoreResquest` - Contains all the api form requests
- `config` - Contains all the application configuration files
- `routes\api` - Contains all the api routes defined in api.php file
- `tests` - Contains all the application tests
- `tests/Feature/ArticleTest` - Contains all the api tests

## Environment variables

- `.env` - Environment variables can be set in this file

# Testing API

The api can now be accessed at

    http://joyjet.loc/api/v1/

Request headers

| **Required** 	| **Key**              	| **Value**            	|
|----------	|------------------	|------------------	|
| Yes      	| Content-Type     	| application/json 	|

Refer the [api specification](#api-specification) for more info.

----------
