<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CartCheckoutResquest;

use Libraries\CartCheckout;

 /**
 * This controller will handle the json payload sata received thru a api endpoint and
 * it will return a the total of each cart in json format
 */
class CartCheckoutController extends Controller
{
    /**
     * Receive the payload data from method POST and return with the Total of each Cart.
     *
     * @param  App\Http\Requests\CartCheckoutStoreResquest  $request
     * @return \Illuminate\Http\Response on json format
     *
     */
    public function cart_checkout(CartCheckoutResquest $request)
    {
        //Data Content Received and Validated
        $data = json_decode($request->getContent(), true);

        $output = new CartCheckout($data);

        return response()->json( ['carts' => $output->carts_subtotal()] , 201);

    }

}
