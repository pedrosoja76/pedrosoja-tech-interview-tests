<?php

namespace Libraries;

/**
 * CartCheckout
 *
 * Handles payload receive thru api
 *
 * @class       CartCheckout
 * @version     2.1.1
 *
 */
class CartCheckout{

    /**
	 * Articles to be handle.
	 *
	 * @since 2.1.1
	 * @var array
	 */
    private array $articles = [];

    /**
	 * Carts to be handle.
	 *
	 * @since 2.1.1
	 * @var array
	 */
    private array $carts = [];

    /**
	 * Delivery Fees indicate on the payload.
	 *
	 * @since 2.1.1
	 * @var array
	 */
    private array $delivery_fees = [];

    /**
	 * Discounts indicate on the payload.
	 *
	 * @since 2.1.1
	 * @var array
	 */
    private array $discounts = [];


    /**
	 * Get the articles and carts passed thru the payload data, otherwise return bad status.
	 *
	 * @param  array $data Data receive from payload to read.
	 */
    public function __construct(array $payload) {

        $this->payload_keys($payload);

    }

    /*
	|--------------------------------------------------------------------------
	| Getters and Setters
	|--------------------------------------------------------------------------
	*/

    /**
	 * Get Articles.
     * This function will get all articles received by the payload.
     *
     * @param int $article_id ID of the article to get
     * @param string $article_field Field of the article to get
	 * @return $result Return the data intended.
     *
	 */
    public function getArticles( int $article_id, string $article_field){

        foreach($this->articles as $article){
            if( $article['id'] === $article_id){
                $result = $article[$article_field];
            }
        }

        return $result;
    }

    /**
	 * Set Articles.
	 *
	 * @since 2.1.1
	 * @param array $articles Articles to set.
     *
	 */
    public function setArticles( array $articles ){
        $this->articles = $articles;
    }


    /**
	 * Get Carts.
	 *
	 * @return array
     *
	 */
    public function getCarts(){
        return $this->carts;
    }

    /**
	 * Set Carts.
	 *
	 * @since 2.1.1
	 * @param array $carts Carts to set.
     *
	 */
    public function setCarts( array $carts ){
        $this->carts = $carts;
    }

    /**
	 * Get Delivery Fees.
	 *
	 * @return array
     *
	 */
    public function getDeliveryFees(){
        return $this->delivery_fees;
    }

    /**
	 * Set Delivery Fees.
	 *
	 * @since 2.1.1
	 * @param array $delivery_fees Delivery Fees to set.
     *
	 */
    public function setDeliveryFees( array $delivery_fees ){
        $this->delivery_fees = $delivery_fees;
    }

    /**
	 * Get Discounts.
	 *
     * @param int $article_id - ID of the article to get
	 * @return float Return the discount to be deduct of the price
     *
	 */
    public function getDiscounts($article_id){

        foreach($this->discounts as $article){
            if( $article['article_id'] === $article_id){
                if( $article['type'] === 'amount'){
                    return (float) $article['value'];
                }

                if( $article['type'] === 'percentage'){
                    $article_price = $this->getArticles( $article_id, 'price');
                    return (float) $article_price * ($article['value']/100);
                }

            }
        }
     }

    /**
	 * Set Discounts.
	 *
	 * @since 2.1.1
	 * @param array $discounts Discounts to set.
     *
	 */
    public function setDiscounts( array $discounts ){
        $this->discounts = $discounts;
     }


    /*
	|--------------------------------------------------------------------------
	| Carts Handling
	|--------------------------------------------------------------------------
	|
	| Verify the carts content and apply, if necessary, the Delivery Fee and Discount.
	*/

    /**
	 * Identify the content of each cart inside the Carts list and calculate the subtotal for each one.
	 *
	 * @return array $output Return the subtotal for each cart.
	 */
    public function carts_subtotal(){

        //Separate cart inside the carts
        foreach($this->getCarts() as $cart_id){

            //Initialize subtotal variable for each cart
            $subtotal = 0.00;
            $delivery_fee = 0.00;

            foreach($cart_id['items'] as $items){

                //Initialize variables
                $article_id = null;
                $quantity = 0;
                $article_price = 0.00;
                $article_discount = 0.00;

                //Create field variables based on payload fields
                $article_id = $items['article_id'];
                $quantity = (int) $items['quantity'];
                $article_price =  (float) $this->getArticles($article_id, 'price');

                //Discount
                $article_discount = $this->getDiscounts($article_id);

                //Calculate the subtotal
                $subtotal += $quantity * ( $article_price - $article_discount);
            }

            //Calculate the Delivery Fee
            $delivery_fee = (float) $this->deliveryFees($subtotal);

            //Calculate the Total
            $total = round( ($subtotal +  $delivery_fee), 2);


            $carts[] = [
                'cart_id' => $cart_id['id'],
                'total' => $total
            ];
        }

        return $carts;
    }

    /*
	|--------------------------------------------------------------------------
	| Payload Handling
	|--------------------------------------------------------------------------
	|
	| Verify in the payload the Articles, Carts, Delivery Fee and Discount.
	*/

    /**
	 * Identify which elements were sent thru the payload.
	 *
	 * @param array $payload Payload receive thry the Api. Default null.
	 */
    public function payload_keys ( array $payload = null ){

        //Verify which keys are avaliable on the payload
        foreach(array_keys($payload) as $key){

            //Search for the Articles
            if($key === 'articles'){
                $this->setArticles($payload['articles']);
            }

            //Search for the Carts
            if($key === 'carts'){
                $this->setCarts($payload['carts']);
            }

            //Search for the Delivery Fees
            if($key === 'delivery_fees'){
                $this->setDeliveryFees($payload['delivery_fees']);
            }

            //Search for the Discounts
            if($key === 'discounts'){
                $this->setDiscounts($payload['discounts']);
            }
        }
    }

    /**
     * This function will verify the delivery fee to be charge based on the total order
     *
     * @param $subtotal - Inform the order subtotal.
     * @return $delivery_fee - Return the delivery fee based on the order subtotal
     *
     */
    public function deliveryFees( float $subtotal){

        //Initialize variable
        $delivery_fee = 0.00;

        foreach($this->getDeliveryFees() as $obj){
            $price = (float) $obj['price'];
            $min_price = (float) $obj['eligible_transaction_volume']['min_price'];
            $max_price = (float) $obj['eligible_transaction_volume']['max_price'];

            if( $subtotal > $min_price && $subtotal <= $max_price){
                $delivery_fee = $price;
            } elseif ( $subtotal >= $min_price ){
                $delivery_fee = $price;
            }
        }

        return (float) $delivery_fee;
    }

}
