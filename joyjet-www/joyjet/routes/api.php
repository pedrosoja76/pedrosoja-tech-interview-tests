<?php

use Illuminate\Support\Facades\Route;

//Api endpoint for Cart Checkout
Route::post('cartcheckout', 'App\Http\Controllers\Api\CartCheckoutController@cart_checkout')->name('cartcheckout.endpoint');
