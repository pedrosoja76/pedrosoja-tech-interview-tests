<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CartCheckoutFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'price' => sprintf("%02d", mt_rand(1, 999)),
        ];
    }
}
