<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Article;
use App\Models\Cart;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // $article = Article::Create([
        //     'name' => Str::random(200),
        //     'price' => sprintf("%02d", mt_rand(1, 999)),
        // ]);

        // Cart::Create([
        //     'article_id' => $article->id,
        //     'quantity' => mt_rand(1, 99),
        // ]);

        Article::factory()->count(10)->create()->each(function($article){
            Cart::Create([
                'article_id' => $article->id,
                'quantity' => mt_rand(1, 99),
            ]);
        }

        );


    }
}
