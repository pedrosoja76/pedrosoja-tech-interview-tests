<?php

namespace Tests\Feature;


use Tests\TestCase;


class CartCheckoutTest extends TestCase
{

    /**
     * A basic feature test for Cart Checkout.
     *
     * @test
     *
     */
    public function test_cartcheckout()
    {

        //Preparation / Prepare
        //CartCheckout::factory()->create();


        // $data = [
        //     "articles"=> [
        //       [ "id"=> 1, "name"=> "water", "price"=> 100 ],
        //       [ "id"=> 2, "name"=> "honey", "price"=> 200 ],
        //       [ "id"=> 3, "name"=> "mango", "price"=> 400 ],
        //       [ "id"=> 4, "name"=> "tea", "price"=> 1000 ]
        //     ],
        //     "carts"=> [
        //       [
        //         "id"=> 1,
        //         "items"=> [
        //           [ "article_id"=> 1, "quantity"=> 6 ],
        //           [ "article_id"=> 2, "quantity"=> 2 ],
        //           [ "article_id"=> 4, "quantity"=> 1 ]
        //         ]
        //       ],
        //       [
        //         "id"=> 2,
        //         "items"=> [
        //           [ "article_id"=> 2, "quantity"=> 1 ],
        //           [ "article_id"=> 3, "quantity"=> 3 ]
        //         ]
        //       ],
        //       [
        //         "id"=> 3,
        //         "items"=> []
        //       ]
        //     ]
        //   ];

          $data = [
            'articles' => [
              0 => [
                'id' => 1,
                'name' => 'water',
                'price' => 100,
              ],
              1 => [
                'id' => 2,
                'name' => 'honey',
                'price' => 200,
              ],
              2 => [
                'id' => 3,
                'name' => 'mango',
                'price' => 400,
              ],
              3 => [
                'id' => 4,
                'name' => 'tea',
                'price' => 1000,
              ],
              4 => [
                'id' => 5,
                'name' => 'ketchup',
                'price' => 999,
              ],
              5 => [
                'id' => 6,
                'name' => 'mayonnaise',
                'price' => 999,
              ],
              6 => [
                'id' => 7,
                'name' => 'fries',
                'price' => 378,
              ],
              7 => [
                'id' => 8,
                'name' => 'ham',
                'price' => 147,
              ],
            ],
            'carts' => [
              0 => [
                'id' => 1,
                'items' => [
                  0 => [
                    'article_id' => 1,
                    'quantity' => 6,
                  ],
                  1 => [
                    'article_id' => 2,
                    'quantity' => 2,
                  ],
                  2 => [
                    'article_id' => 4,
                    'quantity' => 1,
                  ],
                ],
              ],
              1 => [
                'id' => 2,
                'items' => [
                  0 => [
                    'article_id' => 2,
                    'quantity' => 1,
                  ],
                  1 => [
                    'article_id' => 3,
                    'quantity' => 3,
                  ],
                ],
              ],
              2 => [
                'id' => 3,
                'items' => [
                  0 => [
                    'article_id' => 5,
                    'quantity' => 1,
                  ],
                  1 => [
                    'article_id' => 6,
                    'quantity' => 1,
                  ],
                ],
              ],
              3 => [
                'id' => 4,
                'items' => [
                  0 => [
                    'article_id' => 7,
                    'quantity' => 1,
                  ],
                ],
              ],
              4 => [
                'id' => 5,
                'items' => [
                  0 => [
                    'article_id' => 8,
                    'quantity' => 3,
                  ],
                ],
              ],
            ],
            'delivery_fees' => [
              0 => [
                'eligible_transaction_volume' => [
                  'min_price' => 0,
                  'max_price' => 1000,
                ],
                'price' => 800,
              ],
              1 => [
                'eligible_transaction_volume' => [
                  'min_price' => 1000,
                  'max_price' => 2000,
                ],
                'price' => 400,
              ],
              2 => [
                'eligible_transaction_volume' => [
                  'min_price' => 2000,
                  'max_price' => NULL,
                ],
                'price' => 0,
              ],
            ],
            'discounts' => [
              0 => [
                'article_id' => 2,
                'type' => 'amount',
                'value' => 25,
              ],
              1 => [
                'article_id' => 5,
                'type' => 'percentage',
                'value' => 30,
              ],
              2 => [
                'article_id' => 6,
                'type' => 'percentage',
                'value' => 30,
              ],
              3 => [
                'article_id' => 7,
                'type' => 'percentage',
                'value' => 25,
              ],
              4 => [
                'article_id' => 8,
                'type' => 'percentage',
                'value' => 10,
              ],
            ],
        ];

        //Actions / Perform
        $response = $this->postJson(route('cartcheckout.endpoint'), $data);

        //Assertion / Predict
        $this->assertEquals(1, count($response->json()));


        // $this->json('POST', 'api/v1/articles', $data, ['Accept' => 'application/json'])
        //     ->assertStatus(201)
        //     ->assertJson([
        //         "data" => $data,
        //     ]);


    }
}
